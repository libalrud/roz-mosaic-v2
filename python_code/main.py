import sys
import traceback

try:
    import utils
    import user_code
    from user_code import features
except ModuleNotFoundError | ImportError | ImportWarning as err:
    print(f"Error! {err=}", file=sys.stderr)
    traceback.print_tb(err.__traceback__)

feature_calculator = None


def main(input_list: list[list[list[float | int]]], nclust: int, params: tuple):
    """
    Entry point for the application.
    Wraps the user_code call with time spent calculations and debug prints
    To configure inputs and logging settings, check helper_functions/config.py
    input_list: a 3D list of image "layers", typically 3x512x512
    nclust: the number of textures hidden in the image
    """
    global feature_calculator

    print(f"Python starting...", file=sys.stderr)

    try:
        utils.eprint(
            f"Python got: dim: {len(input_list)} x {len(input_list[0])} x {len(input_list[0][0])}. "
            f"{nclust=}, {params=}")

        start_time = utils.start_measuring()

        input_rows = len(input_list[0])
        input_columns = len(input_list[0][0])
        input_color_count = len(input_list)

        if feature_calculator is None:
            feature_calculator = features.Features(rows=input_rows, columns=input_columns,
                                                   color_count=input_color_count,
                                                   params=params)

        # the calculation is split from initialization to allow calculating multiple times easily
        output_list = feature_calculator.calculate_features(image=input_list, cluster_count=nclust,
                                                            image_number=utils.times_count)

        utils.eprint(
            f"Python finished. Returning array {len(output_list)}x{len(output_list[0])}x{len(output_list[0][0])}")

        one_time, total_time, total_py_time, times_count = utils.end_measuring()

        utils.eprint(f"Time spent for image #{times_count}: {one_time:.3f}, {total_time:.3f} total. "
                     f"Python {total_py_time / total_time:.1%}.")

        return output_list

    except Exception as e:
        print(f"Error! {e=}", file=sys.stderr)
        traceback.print_tb(e.__traceback__)
        return input_list
    finally:
        print(f"...Python finished", file=sys.stderr)
