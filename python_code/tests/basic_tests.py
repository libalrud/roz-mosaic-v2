from unittest import *
import traceback

from ..user_code import features
from . import image_loaded


class TestFeatureCalculation(TestCase):
    """
    Stub file for tests of the feature calculation
    test_image is a whole preloaded image -- tests circumvent the need for the bash and cpp components
    """

    features = features.Features(image_loaded.test_image,
                                 512, 512, 3, 3,
                                 image_number=1, params=())
    result = None

    def get_result(self):
        if self.result is None:
            self.result = self.features.calculate_features()
        return self.result

    def test_operation(self):

        try:
            a = self.get_result()
            self.assertIsNotNone(a)
        except BaseException as e:
            print(f"Exception! {e=}")
            traceback.print_exc()
            raise e

    def test_result_dim(self):
        """Test if returned dimensions are correct"""

        result = self.get_result()

        a = len(result)
        b = len(result[0])
        c = len(result[0][0])

        self.assertLess(a, 20)
        self.assertEquals(b, 512)
        self.assertEquals(c, 512)
