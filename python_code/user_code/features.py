import sys


class Features:
    """
    A wrapper class to calculate image features.
    Note: if C is throwing segfaults, please check your imports.
    Debugging imports is possible by adding them in a try block.
    """

    def __init__(self, rows: int, columns: int,
                 color_count: int, color_range: float = 256.0,
                 params: tuple = tuple()):
        """
        color_count is typically 3(RGB), rows and columns probably 512
        color_range represents the maximum color value and is 256 by default but can be changed.
        params delimit the CONSTANT (non learned) params
        """

        # static values
        """Number of rows and columns of the image"""
        self.rows = rows
        self.cols = columns
        self.area = rows * columns

        """Number of color layers and the value range of the colors."""
        self.color_count = color_count
        self.color_range = color_range

        """The raw string parameters as inputted"""
        # process the parameters
        self.params = params

        # set up learned parameters
        # ...

        print("CONSTRUCTED", file=sys.stderr)

    def calculate_features(self, image: list[list[list[float | int]]], cluster_count: int, image_number: int) \
            -> list[list[list[float]]]:
        """
        Return calculated features
        Must be in the form of number of features x width x height, for example 3 x 512 x 512
        e.g. outermost list has one 2D list for each feature
        image is a color_count x rows x columns list. Return an n x rows x columns list of (n) features
        cluster_count is the number of textures hidden in the result image
        image_number is the 0-indexed n of the n-th processed image (useful for testing)
        """

        # output to stdout is filtered and can only be seen using seg.sh's verbose mode or checking the logs.
        # output to stderr is shown as usual
        print(f"Calculating image: {image_number}", file=sys.stderr)

        # edit this
        return image  # returns a set of 3 features: red value, green value, blue value
