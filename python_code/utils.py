import sys
import time

"""if true, prints to stderr, else prints to stdin (the seg.sh script filters out stdin by default)"""
verbose = True


def eprint(*args, **kwargs):
    if verbose:
        print(*args, file=sys.stderr, **kwargs)
    else:
        print(*args, file=sys.stdout, **kwargs)


start_time = time.time()
current_start_time = time.time()
sum_of_times = 0
times_count = 0


def start_measuring():
    global current_start_time, times_count
    current_start_time = time.time()
    times_count += 1
    return


def end_measuring():
    global current_start_time, sum_of_times, start_time, times_count

    end_time = time.time()
    one_time = end_time - current_start_time
    total_time = end_time - start_time

    sum_of_times += one_time

    return one_time, total_time, sum_of_times, times_count




