#!/bin/bash

# this is a mess not meant to be understood.
# sorry

git=0 # 0 - dont git, 1 - also git, 2 - only git
msg=""
help=0
verb=0
subs=0
init=0
init_opt=""

set +m

while getopts ":hilkbszfvtqM:" opt; do
    case $opt in
        h)
            help=1
            ;;
        i) # init mode instead
            echo "* Initialization mode with param $2";
            init_opt=${2:-"NONE"}
            init=1
            break
            ;;
        l) # init mode instead
            echo "* Last time, on segmenter:"
            echo "* =========================================="
            cat segmenter.log
            echo "* =========================================="
            exit
            ;;
        k) # init mode instead
            pkill -f segmenter
            exit
            ;;
        M) # only commit
            git=2
            msg=${OPTARG}
            ;;
        s) # subset
            subs=1
            ;;
        z) # sub-subset
            subs=2
            ;;
        q) # main set, parallel in 2 threads
            subs=3
            ;;
        b) # subset
            subs=4
            ;;
        f) # subset but parallel
            subs=5
            ;;
        t) # test
            subs=6
            ;;
        v) # verbose
            verb=1
            ;;
        :)
            echo "* Error: option ${OPTARG} requires an argument."
            exit
            ;;
        ?)
            echo "* Invalid option: ${OPTARG}. See -h for help."
            exit
            ;;
    esac
done

shift $((OPTIND-1))

if [ $help == 1 ]; then
  echo "HELP"
  echo ""
  echo "Syntax: ./seg.sh <PARAMS> <OPTARGS>"
  echo ""
  echo "Runs segmenter (compiles if necessary) and zips /results folder automatically. Filters log."
  echo "By default, takes full 80 image set, and creates a zip file with results."
  echo "<PARAMS> will be sent as-is to Python"
  echo ""
  echo "-h: see help"
  echo "-v: verbose -- don't filter output"
  echo "-l: show full log from last run"
  echo "-z: test case: only 1 image"
  echo "-s: simple: only 5 images"
  echo "-q: parallel processing of main 80 image set, 2 threads"
  echo "-t: parallel processing of main 80 image set, 10 threads (warning: memory intensive!)"
  echo "-f: parallel processing of simple (-s) 5 image set, 5 threads (warning: memory intensive!)"
  echo "-b: test case: real images from nature in bsds/"
  exit
fi

if [ $init == 1 ]; then

    case $init_opt in
    "ubuntu20.04")
      wget https://mosaic.utia.cas.cz/cvut/1.5/libvr.ubuntu20.04-64.a -O vr/libvr.a --no-check-certificate
      ;;
    "ubuntu18")
      wget https://mosaic.utia.cas.cz/cvut/1.5/libvr.ubuntu18-64.a -O vr/libvr.a --no-check-certificate
      ;;
    "cygwin")
      wget https://mosaic.utia.cas.cz/cvut/1.5/libvr.cygwin-32.a -O vr/libvr.a --no-check-certificate
      ;;
    "fedora")
      wget https://mosaic.utia.cas.cz/cvut/1.5/libvr.fedora-64.a -O vr/libvr.a --no-check-certificate
      ;;
    "bookworm")
      wget https://mosaic.utia.cas.cz/cvut/1.5/libvr.debian-64.bookworm.a -O vr/libvr.a --no-check-certificate
      ;;
    "bullseye")
      wget https://mosaic.utia.cas.cz/cvut/1.5/libvr.debian-64.11.5.a -O vr/libvr.a --no-check-certificate
      ;;
    *)
      echo "-i allows to switch out the precompiled libvr.a version 1.5 for other variants."
      echo "-i ubuntu20.04 -- Ubuntu 20.04 64-bit (and newer, presumably)"
      echo "-i ubuntu18 -- Ubuntu 18 64-bit"
      echo "-i cygwin -- Cygwin 32-bit"
      echo "-i fedora -- Fedora 32 64-bit"
      echo "-i bookworm -- Debian Bookworm/sid 64-bit"
      echo "-i bullseye -- Debian Bullseye 64-bit"
      echo "These have been in no way tested, except Ubuntu 20.04."
      echo "For other options please check the installation page."
      ;;
    esac

    exit
fi

if [ $git -le 1 ]; then
  {
    echo "* Segmenter log:";
    >&2 echo "* Preparing..."

    rm results/*.png
    mkdir -p results # if not exists for some reason


    >&2 echo "* Compiling..."
    make

    if [ $? == 2 ]; then
        exit
    fi

    >&2 echo "Running segmenter...";

    if [ $subs == 0 ]; then # normal
      >&2 echo "* Dataset: colour_large.xml (full 80 images)"
      ./segmenter xml/colour_large.xml "$@" ;

    elif [ $subs == 1 ]; then # -s
      >&2 echo "* Dataset: segmenter.xml (5 images)"
      ./segmenter xml/segmenter.xml "$@" # -s

    elif [ $subs == 2 ]; then # -z
      >&2 echo "* Dataset: single_data.xml (1 image)"
      ./segmenter xml/single_data.xml "$@"

    elif [ $subs == 3 ]; then # -q
      >&2 echo "* Dataset: par2/parX.xml (full 80 images, parallel 2 threads)"

      ./segmenter xml/par2/par1.xml "$@" &
      ./segmenter xml/par2/par2.xml "$@" &

      FAIL=0

      for job in $(jobs -p); do
        >&2 echo "* $job"
        wait "$job" || (("FAIL+=1"))
      done

      if [ "$FAIL" == "0" ]; then
        >&2 echo "* Parallel part done!"
      else
        >&2 echo "* Parallel failed! ($FAIL failures)"
        #exit
      fi

    elif [ $subs == 4 ]; then # -b
      >&2 echo "* Dataset: bsds.xml (12 images from nature)"
      ./segmenter xml/bsds.xml "$@"

    elif [ $subs == 5 ]; then # -f
      >&2 echo "* Dataset: segmenter.xml (5 images) but parallel in 5 threads"

      ./segmenter xml/single_par_5/par1.xml "$@" &
      ./segmenter xml/single_par_5/par2.xml "$@" &
      ./segmenter xml/single_par_5/par3.xml "$@" &
      ./segmenter xml/single_par_5/par4.xml "$@" &
      ./segmenter xml/single_par_5/par5.xml "$@" &

      FAIL=0
      for job in $(jobs -p); do
        >&2 echo "* $job"
        wait "$job" || (("FAIL+=1"))
      done
      if [ "$FAIL" == "0" ]; then
        >&2 echo "* Parallel part done!"
      else
        >&2 echo "* Parallel failed! ($FAIL failures)"
        #exit
      fi

    elif [ $subs == 6 ]; then # -t
      >&2 echo "* Dataset: par10/parX.xml (full 80 images, parallel 10 threads)"

      ./segmenter xml/par10/par1.xml "$@" &
      ./segmenter xml/par10/par2.xml "$@" &
      ./segmenter xml/par10/par3.xml "$@" &
      ./segmenter xml/par10/par4.xml "$@" &
      ./segmenter xml/par10/par5.xml "$@" &
      ./segmenter xml/par10/par6.xml "$@" &
      ./segmenter xml/par10/par7.xml "$@" &
      ./segmenter xml/par10/par8.xml "$@" &
      ./segmenter xml/par10/par9.xml "$@" &
      ./segmenter xml/par10/par10.xml "$@" &

      FAIL=0
      for job in $(jobs -p); do
        >&2 echo "* $job"
        wait "$job" || (("FAIL+=1"))
      done
      if [ "$FAIL" == "0" ]; then
        >&2 echo "* Parallel part done!"
      else
        >&2 echo "* Parallel failed! ($FAIL failures)"
        #exit
      fi

    else
      >&2 echo "* Dataset: ???"
    fi

    rm results.zip;
    zip results.zip -j data/data.xml results/seg*.png;
  } > segmenter.log
  echo "* Segmenter finished with return code $?"
fi

pkill -f segmenter # kill any remaining processes if ended early

if [ $git -ge 1 ] ; then
  echo "* pushing to git with commit message: $msg";
  {
    git add .;
    git commit -m "$msg";
    git push;
  } >> segmenter.log 2>&1
fi

if [ $verb == 1 ] ; then
  echo "* verbose log:";
  echo "* =========================================="
  cat segmenter.log
  echo "* =========================================="
else
  echo "* (Rerun with -l to show log.)"
fi

#echo "* Remaining args are: <${@}>"
