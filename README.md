
# About this project

The [NI-ROZ](https://courses.fit.cvut.cz/NI-ROZ/) semester work is designing a feature calculation for [image texture segmentation](https://mosaic.utia.cas.cz/) using K-means. In C++...

**Don't like C++? Want to skip 3+ hours of setup work? Use this.**
- no need to set up project, download LibVR, default data set, fix the C++ template, etc.,
- you get to use Python, and
- a handy script to help you out!

This is **not** meant for any advanced research. The sole purpose of this project is to lower the entry bar for beginners, 
by providing an easy-to-setup, easy-to-use playing field for experimentation. Do whatever you want with this code. I hope it helps.

### LibVR disclaimer

Part of the source code ([libvr.a](https://mosaic.utia.cas.cz/cvut2022_zimni.html)) is pre-compiled and not modifiable. 
That sucks, but also, Mosaic requires you to download it separately, allegedly for copyright purposes(?) 
It is nevertheless included in this template by default.

### Future

_Future plans include a setup for feature engineering / optimization procedures (currently none) using GT files, 
and visualization using MatPlotLib. Coming soon™..._

## Installation and setup

[//]: # (LibVR:)

[//]: # (- The used LibVR version is [libvr.ubuntu20.04-64.a]&#40;https://mosaic.utia.cas.cz/cvut/1.5/libvr.ubuntu20.04-64.a&#41; from [the segmenter v1.5]&#40;https://mosaic.utia.cas.cz/cvut2022_zimni.html&#41;)


Requirements: 
- 64-bit Ubuntu 20.04+ (*)
- Python 3.10+
- Makefile

[//]: # (- please let me know if I forgot something)

```bash
git clone git@gitlab.fit.cvut.cz:libalrud/roz-mosaic-v2.git --single-branch
cd roz-mosaic-v2 # python-template is the default branch.
```

(*): other versions of `libvr.a` 1.5 are available. They can be auto-downloaded with the `-i` option on the seg script.
These have not been tested to work. Please let me know if they do :)

```bash
./seg.sh -i # lists all libvr variant downloads
./seg.sh -i cygwin # downloads cygwin version of libvr.a and sets it up
```

### Tutorial for registration 

As of writing (Apr 2024) the latest version is the [2022 version](https://mosaic.utia.cas.cz/cvut2022_zimni.html). 
Use a newer one if required by NI-ROZ.

1) go to registration form ([2022 version](https://mosaic.utia.cas.cz/index.php?act=reg_form&vis=22), you may have a newer one. Or use the [non-CVUT registration](https://mosaic.utia.cas.cz/index.php?act=reg_form).)   
2) fill form in with anything (it doesn't matter). 
3) go to register a new 'algorithm' for your account ([2022 version](https://mosaic.utia.cas.cz/index.php?act=alg_form))
4) fill form in with anything (it doesn't matter), just make sure to check the `f3` box.  
5) (optional) further reading ([20?? version](https://courses.fit.cvut.cz/NI-ROZ/tutorials/02/23ROZ_C2.pdf))   


## Usage

1) Edit `python_code/user_code/features.py` (see below)
2) Run segmenter script (see below)
3) Upload results (see below)

### `features.py`

Go to the `Features` class in `python_code/user_code/features.py`.

All you need to do is edit the `calculate_features` method. It accepts three parameters: 
- `image` (as a list of 3 lists of lists of floats) -- each color represents one "layer" or outermost list. 
All inner lists are equal in size (typically 512 x 512). Range < 0.0, 256.0 ) by default.
- `cluster_count` (int) -- how many clusters the image has
- `image_number` (int) -- n for the n-th processed image, 0-indexed

The `Features` class it is in contains some useful data too:
- `self.rows` -- height of the image (typically 512)
- `self.columns` -- width of the image (typically 512)
- `self.color_count` -- how many color layers in the input (typically 3)
- `self.color_range` -- maximum value of a color, not inclusive (typically 256.0)
- `self.params` -- all additional parameters passed to `seg.sh`

#### First steps

A few basic improvements are:
- converting to [numpy](https://numpy.org/install/) ndarray, 
- normalizing the color value range to < 0, 1 >,
- transposing the nested list to represent pixels rather than color layers,
- adding a bit of Gaussian blur with numpy.

### `seg.sh` helper script

The `seg.sh` script allows 
- customized segmenter operation (see `-h`), 
- passing parameters to the feature calculation,
- filtering output (LibVR has some unremovable stdout spam),
- timing each image processing time,
- automatically preparing a zip file to [upload to Mosaic](https://mosaic.utia.cas.cz/index.php?act=segm_form)

`STDOUT` is filtered and only saved into a log, unless verbose mode `-v` is used. 
Use `-l` to show the full log from the previous run.
  
`STDERR` is shown normally.

```bash
./seg.sh <PARAMS> # this runs the whole project. 
./seg.sh <OPTIONS> <PARAMS> # there are several prepared OPTIONS
./seg.sh -h  # see -h for more info about OPTIONS (smaller sets, parallelization, output filtering etc.)
```

The `Features` class will receive `PARAMS` as a tuple of strings in `self.params` (`PARAMS` must not contain `-`'s).
Results from 'results/' are automatically zipped into 'results.zip'.

```bash
./seg.sh -s PARAM1 PARAM2 # example: -s = testing set of 5 images, self.params=('PARAM1', 'PARAM2', )
```

### Upload

1) run the seg.sh script with desired parameters (may take a few minutes)
2) go to the [Mosaic upload page](https://mosaic.utia.cas.cz/index.php?act=segm_form)  
3) upload `results.zip`, select your algorithm from earlier, upload! (may take a few minutes)
4) profit

Note: if you upload incomplete results, evaluation will still work for the results.

You can compare the results to previous versions, or see comparison to [Other algorithms](https://mosaic.utia.cas.cz/index.php?act=view_res).
It is possible to filter results for your semester only ([2022 example](https://mosaic.utia.cas.cz/index.php?act=view_res&dyn=-1&hr=0&sort=-1&dir=0&f1=-1&f2=-1&f3=-1&f4=-1&ndl=-1&nt=-2&vis=22))

### Report

A copy of the `NI-ROZ` semester work template from 2015 can be found at `report_template/`

### Troubleshooting

Common git problem: "/bin/bash^M: bad interpreter: No such file or directory" 
Solution:
```bash
sed -i -e 's/\r$//' seg.sh
```

Alternatively, use `dos2unix seg.sh`.

Something else? -> `libalrud@fit.cvut.cz`

----

## Further reading:
- [Archive of past works](https://courses.fit.cvut.cz/MI-ROZ/tutorials/archive/index.html) and the [Report template](https://courses.fit.cvut.cz/MI-ROZ/media/tutorials/02/roz-tz.zip)
- [Explanation of Mosaic result page](https://courses.fit.cvut.cz/NI-ROZ/tutorials/02/23ROZ_C2.pdf)
- [Compare algorithms in NI-ROZ](https://mosaic.utia.cas.cz/index.php?act=view_res&dyn=-1&hr=0&sort=-1&dir=0&f1=-1&f2=-1&f3=-1&f4=-1&ndl=-1&nt=-2&vis=22)
- [Mosaic README file at vr/readme.txt](https://gitlab.fit.cvut.cz/libalrud/roz-mosaic-v2/-/blob/python_template/vr/readme.txt)
- [Tutorials info at Courses](https://courses.fit.cvut.cz/MI-ROZ/tutorials/index.html) (possibly outdated, certainly messy)

<!---

# Setting up a new project (WITHOUT TEMPLATE)

## tutorial for registration: https://mosaic.utia.cas.cz/cvut2022_zimni.html or current year URL  
1) go to https://mosaic.utia.cas.cz/index.php?act=reg_form&vis=22 or current year URL   
2) fill in anything it doesn't matter  
3) go to register new 'algorithm': https://mosaic.utia.cas.cz/index.php?act=alg_form  
4) fill in anything it doesn't matter  
5) (optional) further reading: https://courses.fit.cvut.cz/NI-ROZ/tutorials/02/23ROZ_C2.pdf   
	

## tutorial for setup -- at https://mosaic.utia.cas.cz/cvut2022_zimni.html or current year URL   
1) download segmenter 1.5 `wget -O segmenter.zip https://mosaic.utia.cas.cz/cvut/segmenter.1.5.zip --no-check-certificate`  
2) extract segmenter `unzip segmenter.zip`  
3) remove zip `rm segmenter.zip`  
4) go to directory `cd segmenter/`  
5) download libvr and move to /vr/ and rename to libvr.a `wget -O vr/libvr.a https://mosaic.utia.cas.cz/cvut/1.5/libvr.ubuntu20.04-64.a --no-check-certificate`  
6) download testing data `wget -O data.zip https://mosaic.utia.cas.cz/cvut/CVUT2022_zimni.zip --no-check-certificate`  
7) extract testing data `unzip data.zip -d data/`
8) remove data.zip `rm data.zip`  
9) read data/readme.txt  
10) (optional) `git init` and figure out an upstream  
11) (optional) before you break anything `git commit -m "initial state"`  
  
### Copy and paste code for setup (linux):  
```
wget -O segmenter.zip https://mosaic.utia.cas.cz/cvut/segmenter.1.5.zip --no-check-certificate
unzip segmenter.zip
rm segmenter.zip
cd segmenter/
wget -O vr/libvr.a https://mosaic.utia.cas.cz/cvut/1.5/libvr.ubuntu20.04-64.a --no-check-certificate
wget -O data.zip https://mosaic.utia.cas.cz/cvut/CVUT2022_zimni.zip --no-check-certificate
unzip data.zip -d data/
rm data.zip
```

## tutorial for each run after editing:  

1) edit main.cpp or smth  
2) compile `make`  
3) (optional) run testing set (7 imgs @ /bsds/) `./segmenter bsds.xml`  
4) (optional) run small set (5 imgs @ /data/) `./segmenter segmenter.xml`  
5) run full set (80 imgs @ /data/)\* `./segmenter colour_large.xml`  
6) remove old zip file `rm results.zip`  
7) zip results `zip results.zip -j data/data.xml results/seg*.png`  
8) (optional) git commit  
9) go to https://mosaic.utia.cas.cz/index.php?act=segm_form  
10) upload `results.zip`, select your algorithm from earlier, upload!  
11) check out the cool stuff  
	
\* an empty calculation takes 2-5 seconds per image


### Copy and paste code for setup (linux):  
```
make
./segmenter colour_large.xml > /dev/null
rm results.zip
zip results.zip -j data/data.xml results/seg*.png
```
	

----

# Templates :)

## C template:  

Basic C template. Basically just what the above tutorial says to do. Not maintained.

### Installation

```
git clone git@gitlab.fit.cvut.cz:libalrud/roz-mosaic-v2.git
cd roz-mosaic-v2
git checkout c_baseline_state
```

### Usage

1) Edit `segmenter.cpp` to change feature space.
2) see tutorial above

## Python template

Includes a few handy abstraction layers all around. TODO update

### Installation

```
git clone git@gitlab.fit.cvut.cz:libalrud/roz-mosaic-v2.git
cd roz-mosaic-v2
git checkout python_baseline_state
```

### Usage

1) Edit Features class in `user_code/features.py` and/or add new scripts to change calculated features.
2) Run `./seg.sh`. This compiles the project, executes main training set, zips to file and filters output. See `./seg.sh -h` for help
3) (optional) run with flag `-v` to stop output filtering, or re-run with `-l` to just show what was outputted last time
4) (optional) run with flag `-s` to use smaller set of 5 images

### Upload

See above, here duplicated:
1) go to https://mosaic.utia.cas.cz/index.php?act=segm_form  
2) upload `results.zip`, select your algorithm from earlier, upload!  
3) check out the cool stuff  


ps. [if LAB color space is required, check this out](https://stackoverflow.com/questions/37504857/convert-rgb-triplets-to-lab-triplets-using-skimage-color-rgb2lab)
	

	
	
	
	
	
	
	
	

