# VR CVUT segmenter makefile
# edited for use in mosaic template

all:	segmenter
segmenter:	main.cpp Segmenter.cpp
	@echo
	@echo VR CVUT Segmenter building.
	@echo
	g++ -O -Wall -o segmenter main.cpp Segmenter.cpp -Ivr -Lvr -lvr -lm -ldl -I/usr/include/python3.10 -lpython3.10
	@echo Compilation successfully finished.
	@echo

clean:
	rm -rf *.o
