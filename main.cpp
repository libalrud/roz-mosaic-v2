
/* CVUT 2020 - main file */
/* segmenter v1.5 */


#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "Segmenter.h"
#include "DyXML_wrapper.h"
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>


// Derive your own segmenter class
class MySegmenter : public Segmenter {
//  virtual int features(DyXML_wrapper &par);
//  virtual int clustering(DyXML_wrapper &par);
//  virtual int postprocessing(DyXML_wrapper &par);
};

// Main function - maintains processing loop and parameters
int main(int argc, char* argv[]) {

    Py_InitializeEx(0);
    PyRun_SimpleString("import sys; sys.path.insert(0, 'python_code/')"); //

    DyXML_wrapper par(argc > 2 ? 2 : argc,argv,"Segmenter");    // load parametric file

    par.init_runs();                             // initialize default parameters
    while (par.next_run()) {                     // main loop over all computation runs
        MySegmenter seg;                           // instance of the overriden class

        seg.seg_argc = argc;
        seg.seg_argv = argv;


        seg.run(par);                              // run segmentation
    }

    Py_Finalize();
    return 0;
}
